package com.example.quiz4

import android.app.Activity
import android.net.Uri
import android.util.Log
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.rows.view.*

class Adapter(var numberofows:MutableList<Mymodel>): RecyclerView.Adapter<Adapter.holder>() {

    var symbol="x"
    var counter=0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): holder {
        return holder(LayoutInflater.from(parent.context).inflate(R.layout.rows,parent,false))
    }

    override fun getItemCount(): Int {
        return numberofows.size
    }

    override fun onBindViewHolder(holder: holder, position: Int) {
        holder.onbind()
    }

    inner class holder(itemView: View): RecyclerView.ViewHolder(itemView){



       fun onbind(){
           var model=numberofows[adapterPosition]

           itemView.row.setOnClickListener(){
                if(!model.click && symbol=="y") {
                    itemView.row.setImageResource(R.mipmap.x)
                    symbol="x"
                    model.symbol=symbol
                    model.click=true
                    counter++
                }
               else if(!model.click && symbol=="x"){
                    itemView.row.setImageResource(R.mipmap.o)
                    symbol="y"
                    model.symbol=symbol
                    model.click=true
                    counter++

                }

             //  CheckWinner()
              CheckWinner2()
              chediagonal()

           }


       }

        fun CheckWinner()
        {
            var winner = -1

            //row1
            if (numberofows[0].symbol=="x" && numberofows[1].symbol=="x"  && numberofows[2].symbol=="x" )
            {
                winner = 1
            }
            if (numberofows[0].symbol=="y" && numberofows[1].symbol=="y"  && numberofows[2].symbol=="y" )
            {
                winner = 2
            }

            //row2
            if (numberofows[3].symbol=="x" && numberofows[4].symbol=="x"  && numberofows[5].symbol=="x" )
            {
                winner = 1
            }
            if (numberofows[3].symbol=="y" && numberofows[4].symbol=="y"  && numberofows[5].symbol=="y" )
            {
                winner = 2
            }

            //row3
            if (numberofows[6].symbol=="x" && numberofows[7].symbol=="x"  && numberofows[8].symbol=="x" )
            {
                winner = 1
            }
            if (numberofows[6].symbol=="y" && numberofows[7].symbol=="y"  && numberofows[8].symbol=="y")
            {
                winner = 2
            }

            //col1
            if (numberofows[0].symbol=="x" && numberofows[3].symbol=="x"  && numberofows[6].symbol=="x")
            {
                winner = 1
            }
            if (numberofows[0].symbol=="y" && numberofows[3].symbol=="y"  && numberofows[6].symbol=="y")
            {
                winner = 2
            }

            //col2
            if (numberofows[1].symbol=="x" && numberofows[4].symbol=="x"  && numberofows[7].symbol=="x")
            {
                winner = 1
            }
            if (numberofows[1].symbol=="y" && numberofows[4].symbol=="y"  && numberofows[7].symbol=="y")
            {
                winner = 2
            }

            //col3
            if (numberofows[2].symbol=="x" && numberofows[5].symbol=="x"  && numberofows[8].symbol=="x")
            {
                winner = 1
            }
            if (numberofows[2].symbol=="y" && numberofows[5].symbol=="y"  && numberofows[8].symbol=="y")
            {
                winner = 2
            }

            //cross1
            if (numberofows[0].symbol=="x" && numberofows[4].symbol=="x"  && numberofows[8].symbol=="x")
            {
                winner = 1
            }
            if (numberofows[0].symbol=="y" && numberofows[4].symbol=="y"  && numberofows[8].symbol=="y")
            {
                winner = 2
            }

            //cross2
            if (numberofows[2].symbol=="x" && numberofows[4].symbol=="x"  && numberofows[6].symbol=="x")
            {
                winner = 1
            }
            if (numberofows[2].symbol=="y" && numberofows[4].symbol=="y"  && numberofows[6].symbol=="y")
            {
                winner = 2
            }

            if (winner != -1)
            {
                if (winner == 1)
                {

                        Toast.makeText(itemView.context, "Player 1 Wins!!", Toast.LENGTH_SHORT).show()


                }
                else
                {

                        Toast.makeText(itemView.context, "Player 2 Wins!!", Toast.LENGTH_SHORT).show()

                    }

                }


            }
fun CheckWinner2() {
            var player1 = 0
            var player2 = 0
            var count = 1
            for (x in 0 until numberofows.size step sqrt(numberofows.size.toDouble()).toInt()) {
                //   d("ragaca2","$x")

                for (y in 1 until sqrt(numberofows.size.toDouble()).toInt()) {
                    if (numberofows[x].symbol == numberofows[y].symbol && numberofows[x].symbol != "" && numberofows[y].symbol != "") {
                        count++
                        if (numberofows[x].symbol == "x" && count == sqrt(numberofows.size.toDouble()).toInt()) player1++

                        if (numberofows[x].symbol == "y" && count == sqrt(numberofows.size.toDouble()).toInt()) player2++
                    }


                    /*  d("ragaca2","$player1 ")*/
                }
                count = 1
                if (player1 != 0) Toast.makeText(
                    itemView.context,
                    "player1 wins",
                    Toast.LENGTH_SHORT
                ).show()
                else if (player2 != 0) Toast.makeText(
                    itemView.context,
                    "player2 wins",
                    Toast.LENGTH_SHORT
                ).show()


            }
        }

        fun chediagonal() {

            var player1 = 0
            var player2 = 0
            var count = 1

            for (x in 0 until numberofows.size step sqrt(numberofows.size.toDouble()).toInt()) {
                //   d("ragaca2","$x")

                for (y in sqrt(numberofows.size.toDouble()).toInt() + 1 until numberofows.size step sqrt(
                    numberofows.size.toDouble()
                ).toInt() + 1) {
                    d("trtr", "$x   $y")
                    if (numberofows[x].symbol == numberofows[y].symbol && numberofows[x].symbol != "" && numberofows[y].symbol != "") {
                        count++
                        if (numberofows[x].symbol == "x" && count == sqrt(numberofows.size.toDouble()).toInt()) player1++

                        if (numberofows[x].symbol == "y" && count == sqrt(numberofows.size.toDouble()).toInt()) player2++

                    }


                }
            }
            count = 1
            if (player1 != 0) Toast.makeText(
                itemView.context,
                "player1 wins",
                Toast.LENGTH_SHORT
            ).show()
            else if (player2 != 0) Toast.makeText(
                itemView.context,
                "player2 wins",
                Toast.LENGTH_SHORT
            ).show()


        }




        }

    }
