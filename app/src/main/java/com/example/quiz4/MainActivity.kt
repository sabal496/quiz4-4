package com.example.quiz4

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init(){
        startgamebtn.setOnClickListener(){
            if(numberofros.text.toString().toInt() in 9..25){
                var intent=Intent(this,gameActivity::class.java)
                intent.putExtra("numberofrow",numberofros.text.toString())
                startActivity(intent)
                finish()
            }
            else{
                Toast.makeText(this,"please enter number from 9 to 20",Toast.LENGTH_SHORT).show()
            }
        }
    }
}
