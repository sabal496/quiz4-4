package com.example.quiz4

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.GridLayout
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_game2.*
import kotlin.math.sqrt

class gameActivity : AppCompatActivity() {

    lateinit var adapter:Adapter
    var datalist= mutableListOf<Mymodel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game2)
        init()

    }

    private fun init(){
         var number= intent.extras?.getString("numberofrow").toString().toInt()
        Toast.makeText(this,"$number",Toast.LENGTH_SHORT).show()

        var numberofrows= sqrt(number.toDouble())

        for (x in 0 until number){
            datalist.add(Mymodel(false))
        }

        adapter= Adapter(datalist)
        recycle.layoutManager= GridLayoutManager(this,numberofrows.toInt())
        recycle.adapter=adapter


        restart.setOnClickListener(){
            var inte=Intent(this,MainActivity::class.java)
            startActivity(inte)
            finish()

        }
    }



}
